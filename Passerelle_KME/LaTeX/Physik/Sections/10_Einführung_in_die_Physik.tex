\clearpage % flushes out all floats
\section{Einführung in die Physik}
\subsection{Was ist Physik?}

Die Physik untersucht die grundlegenden Phänomene der unbelebten Natur -- mit den kleinsten im Bereich der Elementarteilchen bis zu den grössten des Universums -- und versucht 
    deren Eigenschaften und Verhalten mit \textit{quantitativen} (d.h. mit Zahlen ausdrückbaren) Modellen und Gesetzmässigkeiten zu erklären. Diese mathematisch formulierten Gesetze
    haben zum Ziel, das Verhalten der Natur zu beschreiben und praktische Anwendungen in der Technik zu finden.

\subsection{Messen und Zählen}

Das Messen einer Physikalischen Grösse bedeutet, diese mit etwas Bekanntem zu vergleichen. Genau genommen bestimmt man den Grössenwert als ein Vielfaches eines Vergleichsgegenstandes.
    Den Grössenwert des Vergleichsgegenstandes bezeichnet man als Einheit, den Faktor als Zahlenwert oder Masszahl. Die gemessene Grösse wird als Produkt aus Zahlenwert und Einheit angegeben.

\subsubsection{SI-Einheiten}

Das allgemein in der Wissenschaftswelt verwendete System, in welchem neben dem Meter, Kilogramm und der Sekunden weitere zugrunde liegen, heisst SI-System (\textit{Système International d'unités});
    man spricht von sogenannten \textbf{SI-Einheiten}.

\ % Adds new line

Die grundlegenden Einheiten Einheiten des SI-Systems sind der Meter (\si{\metre}), die Sekunde (\si{\second}), das Kilogramm (\si{\kilo\gram}), das Kelvin (\si{\kelvin}), das Ampère (\si{\ampere}),
    das Mol (\si{\mole}) und Candela (\si{\candela}). Die Einheit jeder physikalischen Grösse kann durch diese sieben Grundeinheiten ausgedrückt werden.

\subsubsection{Vielfache von Einheiten}

\ % Adds new line

\begin{center}
    \begin{minipage}[b]{\linewidth}
        \hspace{4.5em}
            \begin{tabular}{l|l|c} % Alignment of Columns
            Faktor       &   Vorsatz &   Zeichen \\
            \hline
            $10^{-1}$    &   Dezi    &   d       \\
            $10^{-2}$    &   Zenti   &   c       \\
            $10^{-3}$    &   Milli   &   m       \\
            $10^{-6}$    &   Mikro   &   $\mu$   \\
            $10^{-9}$    &   Nano    &   n       \\
            $10^{-12}$   &   Piko    &   p       \\
            $10^{-15}$   &   Femto   &   f       \\
            $10^{-18}$   &   Atto    &   a       \\
            $10^{-21}$   &   Zepto   &   z       \\
            $10^{-24}$   &   Yokto   &   y       \\
            \end{tabular}
        \hspace{\fill}
            \begin{tabular}{l|l|c} % Alignment of Columns
            Faktor     &   Vorsatz &   Zeichen \\
            \hline
            $10^1$     &   Deka    &   da      \\
            $10^2$     &   Hekto   &   h       \\
            $10^3$     &   Kilo    &   k       \\
            $10^6$     &   Mega    &   M       \\
            $10^9$     &   Giga    &   G       \\
            $10^{12}$  &   Tera    &   T       \\
            $10^{15}$  &   Peta    &   P       \\
            $10^{18}$  &   Exa     &   E       \\
            $10^{21}$  &   Zetta   &   Z       \\
            $10^{24}$  &   Yotta   &   Y       \\
            \end{tabular}
        \hspace{4.5em}
    \end{minipage}
\end{center}

\subsubsection{Die wissenschaftliche Schreibweise}

Der Umgang mit sehr grossen und sehr kleinen Zahlen wird durch die sogenannte "wissenschaftliche Schreibweise" \ vereinfacht. In dieser Schreibweise werden die Zahlen als Produkte einer Zahl
    zwischen 1 und 10 und einer Zehnerpotenz geschrieben, wie zum Beispiel: $10^2$(=100) oder $10^4$(=10'000). Im Umkehrschluss wird die Zahl 150'000'000'000\si{\metre} (Abstand von der Sonne zur Erde)
    zu $1.5 \cdot 10^{11}$ umgeschrieben. Die hochgestellte Zahl, hier 11, heisst \textbf{Exponent}, die 10 heisst \textbf{Basis} und die 1.5 heisst \textbf{Matisse}. Für Zahlen kleiner
    als 1 wird der Exponent negativ, beispielsweise ist 0.1 = $10^{-1}$.
    \newpage

\subsubsubsection{Verrechnung von Zahlen in wissenschaftlicher Schreibweise}

Werden Zahlen in wissenschaftlicher Schreibweise multipliziert oder dividiert, so werden die Exponenten addiert beziehungsweise subtrahiert. Zum Beispiel:

    \[ 10^2 \cdot 10^3 = 10^{2+3} = 10^5 = 100'000 \]

    \[ \frac{10^2}{10^3} = 10^{2-3} = 10^{-1} = 0.1 \]

Falls Zahlen addiert oder subtrahiert werden müssen, muss aufgepasst werden, wenn die Exponenten nicht gleich sind. Zuerst müssen beide Zahlen mithilfe derselben Zehnerpotenz geschrieben werden, bevor
    sie addiert oder subtrahiert werden können. Zum Beispiel:

    \[ 1.2 \cdot 10^2 + 8 \cdot 10^{-1} = 1'200 \cdot 10^{-1} + 8 \cdot 10^{-1} = 1'208 \cdot 10^{-1} = 120.8 \]

\subsubsection{Signifikante Stellen und Grössenordnungen}

Fast alle in der Physik auftretenden Zahlen kommen direkt oder indirekt aus einer Messung und sind deshalb \textbf{nur bis auf eine gewisse Messungenauigkeit} bekannt. Die
    Grössenordnung des Messfehlers hängt von der Messapparatur und der Geschicklichkeit des Experimentators ab und kann häufig nur geschätzt werden. \\\hspace*{\fill} % Fixes error underfull \hbox (badness 10'000)
    \newline
    Zwei einfache \textbf{Faustregeln}, wie viele Stellen man angeben soll, lauten wie folgt:
    \hfill \break
    Die Anzahl signifikanter Stellen eines \textit{Produktes} oder eines \textit{Quotienten}
    soll nicht mehr sein, als im Faktor mit der kleinsten Anzahl signifikanter Stellen angegeben sind. Das Resultat einer \textit{Addition} zweier Zahlen sollte keine Stellen über
    die Dezimalstelle hinaus angeben, für welche beide Summanden signifikante Stellen aufweisen.
    \newpage