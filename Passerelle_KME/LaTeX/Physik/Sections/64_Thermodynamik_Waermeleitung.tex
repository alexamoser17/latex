\clearpage % flushes out all floats
\subsection{Die Wärmeleitung}

Wärmetransport bedeutet, dass innere Energie von einem Ort zu einem anderen Ort gelangt. Wärmeübertragung kann auf drei Arten erfolgen: Zusammen mit der Substanz, in der sie gespeichert ist (Wärmeströmung oder 
    Konvektion), durch die Trägersubstanz hindurch (Wärmeleitung) oder ohne eine Trägersubstanz (Wärmestrahlung).

\subsubsection{Die Wärmeströmung (Konvektion)}

Die Wärmeströmung (Konvektion) kann von selber durch temperaturbedingte Dichteunterschiede (wegen dem Temperaturgradient innerhalb des Mediums, z.B. die heissen Gase eines Feuers, die eine geringere Dichte als die 
    Umgebungsluft haben und dadurch Konvektionsströme erzeugen) oder von aussen durch eine Pumpe verursacht werden. \\\hspace*{\fill}

Die Transportrate der Wärmeströmung hängt unter anderem von der Reibungskraft ab. Die Reibungskraft wirkt auf jede Wärmeströmung bremsend und hängt von der Zähigkeit des Materials ab, also von der Viskosität (je 
    Viskoser, desto langsamer sind die Wärmeströmungen). Des Weiteren hängt die Transportrate der Wärmeströmung auch vom Temperaturunterschied ab, denn der Temperaturunterschied bestimmt der Dichteunterschied.

\subsubsection{Wärmeleitung}

Den Übergang der Wärme von heissen Teilen eines Körpers auf benachbarte kältere Stellen nennt man Wärmeleitung. Bei der Wärmeleitung wird keine Materie bewegt, d.h. dieser Übergang ist materialabhängig: Metalle sind 
    gute, Flüssigkeiten und Gase schlechte Wärmeleiter. Die Wärmeleitfähigkeit des Vakuums ist annäherungsweise Null, denn ohne Atome gibt es keinen Transport. \\\hspace*{\fill}

Für die Wärmeleitung kann auch wieder eine Formel, die sogenannte \textbf{Wärmeleitgleichung} hergeleitet werden. Als Grundvoraussetzung haben wir einen Stab der Länge \textit{l}, zwischen dessen beiden Enden eine 
    Temperaturdifferenz von $\Delta T$ herrscht. Für die Wärmetransportrate \textit{P} (Wärmemenge $\Delta Q$, die durch die Querschnittsfläche \textit{A} im Zeitabschnitt $\Delta t$ transportiert wird) gilt:
    \begin{equation}
        P = \frac{\Delta Q}{\Delta t} = -\lambda \cdot A \cdot \frac{\Delta T}{l} \ [\si{\watt}] \label{eq:Wärmeleitung}
    \end{equation}

Die Materialkonstante $\lambda \ \left[\frac{\si{\watt}}{\si{\metre} \cdot \si{\kelvin}}\right]$ drückt die Wärmeleitfähigkeit aus. Das Minuszeichen in der Formel deutet an, dass der Wärmefluss von heiss nach kalt 
    verläuft.

\subsubsection{Wärmestrahlung}

Wärmestrahlung ist ein Mechanismus zum Transport von thermischer Energie von einem Ort zum anderen, für den kein materieller Träger notwendig ist. Wärmestrahlung besteht (wie z.B. das sichtbare Licht) aus 
    elektromagnetischen Wellen, die jeder Körper abhängig von seiner Temperatur emittiert. \\\hspace*{\fill}

Jeder Körper gibt Wärmestrahlung an die Umgebung ab bzw. nimmt sie aus der Umgebung auf (Absorption). Je heisser ein Körper ist, desto intensiver ist die von ihm emittierte Strahlung. Wärmestrahlung kann sich 
    glücklicherweise auch im Vakuum ausbreiten (z.B. Sonnenstrahlen). \\\hspace*{\fill}

Wie schon erwähnt sendet jeder Körper, dessen Temperatur über dem absoluten Nullpunkt liegt, Wärmestrahlung aus. Das folgende \textbf{Stefan-Boltzmann-Gesetz} gibt an, welche Strahlungsintensität von einem Körper 
    mit der absoluten Temperatur \textit{T} ausgeht:
    \begin{equation}
        P = \sigma \cdot A \cdot \epsilon \cdot T^4 \ [\si{\watt}] \label{eq:Wärmestrahlung}
    \end{equation}

Die Stefan-Boltzmann-Konstante $\sigma$ (=Sigma) ist eine Naturkonstante mit dem Wert $5.6704 \cdot 10^{-8}\frac{\si{\watt}}{\si{\metre}^2 \cdot \si{\kelvin}^4}$. \textit{A} ist die Oberfläche des Objektes und das 
    $\epsilon$ gibt den Emissionswert an und wird bei einem schwarzen Körper (z.B. Sonne) mit $1$ angenommen.

\paragraph{Beispielaufgaben}
\subparagraph{Beispiel 1}

\textit{Gegeben:} Ein Kupferstab ($A = 1.8\si{\cm}^2$, $l = 10\si{\cm}$) wird am oberen Ende geheizt und auf $600\si{\kelvin}$ gehalten. Das untere Ende taucht in Eis. \\\hspace*{\fill}

\textit{Gesucht:} Welche Wärmemenge fliesst in 15 Minuten durch den Stab hindurch? \\\hspace*{\fill}

\textit{Lösung:}
\begin{alignat*}{2}
    \frac{\Delta Q}{\Delta t} &= -\lambda \cdot A \cdot \frac{\Delta T}{l} \ &&\Rightarrow \ \Delta Q = -\lambda \cdot A \cdot \frac{\Delta T \cdot \Delta t}{l} \\
                                                                           & &&\Rightarrow \ \Delta Q = -390\left[\frac{\si{\watt}}{\si{\metre} \cdot \si{\kelvin}}\right] \cdot 1.8 \cdot 10^{-4}\si{\metre}^2 \cdot \frac{326.85\si{\kelvin} \cdot 900\si{\second}}{0.1\si{\metre}} \\
                                                                           & &&\Rightarrow \ \Delta Q = \underline{\underline{206'503.83\si{\joule}}}
\end{alignat*}