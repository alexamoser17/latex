% Zusammenfassung und Kurztheorie Stochastik
%
% Quellen: 
% https://de.wikipedia.org/wiki/Wahrscheinlichkeitstheorie
% https://de.wikipedia.org/wiki/Wahrscheinlichkeitsraum
% https://de.wikipedia.org/wiki/Bedingte_Wahrscheinlichkeit
% https://de.wikipedia.org/wiki/Satz_von_Bayes
% https://de.wikipedia.org/wiki/Binomialverteilung
% https://de.wikipedia.org/wiki/Kombination_(Kombinatorik)
% https://de.wikipedia.org/wiki/Permutation
% https://de.wikipedia.org/wiki/Variation_(Kombinatorik)
% Skript Corinna
% Notizen Csaba

%%% PACKAGES %%%
% Standard
\documentclass[a4paper]{article}
\usepackage[utf8]{inputenc}
\usepackage[ngerman]{babel}
\usepackage[T1]{fontenc}


% Formatierung
\usepackage{geometry}           
\usepackage{lastpage}
\usepackage{fancyhdr}
\usepackage[hidelinks]{hyperref}

% Symbole etc.
\usepackage{amsmath}           
\usepackage{amssymb}    % \cdots
\usepackage{cancel}     % Gestrichene Terme

% Graphiken und Plots
\usepackage{tikz}         
\usepackage{pgfplots}

%%% PREAMBLE %%%
% Formatierung und Titel
\geometry{bottom=30mm}
\setlength\parindent{0pt}
\title{\vspace{-3cm}Stochastik: Kurztheorie und Zusammenfassung}
\author{Stefan Gloor}

\pgfplotsset{compat=1.7}                % Version? für Plots     
\numberwithin{equation}{subsubsection}  % Nummerierung Equations

\fancypagestyle{TitlePageStyle}{ %% Titelseite
    \fancyhead{}
    \cfoot{\today}
    \lfoot{Stefan Gloor}
    \rfoot{Seite \thepage \protect{ }von \pageref{LastPage}}
    \renewcommand{\headrulewidth}{0pt}
    \renewcommand{\footrulewidth}{1pt}
}

\cfoot{\today} %% Folgende Seiten
\lfoot{Stefan Gloor}
\rfoot{Seite \thepage \protect{ }von \pageref{LastPage}}
\renewcommand{\headrulewidth}{1pt}
\renewcommand{\footrulewidth}{1pt}
\pagestyle{fancy}

%%% DOCUMENT %%%
\begin{document}
\maketitle

    \thispagestyle{TitlePageStyle}

    \section{Wahrscheinlichkeitsrechnung}
        \subsection{Definitionen}
            \begin{tabular}{cl}
                $\omega$ & Ergebnis eines Versuchs ($\omega \in \Omega$).\\
                $\Omega$ & Ergebnismenge: Menge aller möglichen Ergebnisse.\\
                $|\Omega|$ & Mächtigkeit der Ergebnismenge: Anzahl möglicher Ergebnisse.\\
                $h_n$ & relative Häufigkeit: Bei $n$ Versuchen tritt $\omega_i$ $k$-mal auf, dann gilt $h_n=\frac{k}{n}$.\\
                $A$ & Ereignis: Menge von Ergebnissen.\\
                $\Sigma$ & Ereignismenge, entspricht der Potenzmenge der Ergebnismenge $\Sigma=\mathcal{P}(\Omega)$. \\
                $X(\omega_i)$ & Zufallsgrösse: Jedem Ergebnis kann eine Grösse zugeordnet werden.\\
                $E(X)$ & Erwartungswert: theoretischer Mittelwert der Zufallsgrösse $E(X)=\sum_{i=1}^{n} P(\omega_i)\cdot X(\omega_i)$.\\
                $P(A)$ & Wahrscheinlichkeit eines Ereignisses.\\

            
            \end{tabular}

        \subsection{Laplace-Versuche}
            Alle Ergebnisse haben diesselbe Wahrscheinlichkeit.
            \begin{equation}
                p=\frac{1}{|\Omega|}
            \end{equation}
            Die \textbf{Laplace-Regel} für das Ereignis $A = \{\omega_1,\omega_2,...,\omega_k\}$
            besagt
            \begin{equation}\label{eq:laplaceRule}
                P(A)=\frac{|A|}{|\Omega|}
            \end{equation}
            weil jedes Ergebnis $\omega_i$ des Ereignisses $A$ gleich wahrscheinlich ist.

        \subsection{Einfache Zufallsversuche}
            Ein fairer Spielwürfel mit sechs Seiten besitzt die Ergebnismenge $\Omega=\{1,2,3,4,5,6\}$;
            jedes der sechs Elemente $\omega_i$ kann mit gleicher Wahrscheinlichkeit eintreten. 
            Die Ereignismenge $\Sigma = \mathcal{P}(\Omega)$ und die dazugehörigen Wahrscheinlichkeiten der einzelnen Elemente (= Ereignisse) sind in folgender Tabelle ersichtlich.

            \begin {center}
                \begin{tabular}{c|c}
                    $\Sigma$ & $P$ \\ \hline
                    $\emptyset$ & 0 \\
                    \{1\} & 1/6\\
                    \{2\} & 1/6 \\
                    \{3\} & 1/6 \\
                    \{4\} & 1/6 \\
                    \{5\} & 1/6 \\
                    \{6\} & 1/6 \\
                    \{1,2\} & 2/6 \\
                    \vdots & \vdots \\
                    \{5,6\} & 2/6 \\
                    \{1,2,3\} & 3/6 \\
                    \vdots & \vdots \\
                    \{1,2,3,4,5,6\} & 1 \\

                \end{tabular}\\
            \end{center}

            Häufig sind nur die Wahrscheinlichkeiten für \textbf{Elementarereignisse} wie z.B. $P(\{2\})=1/6$ bekannt. 
            Die Wahrscheinlichkeiten für andere Ereignisse $\in \Sigma$ lassen sich aber durch einfache Mengenoperationen ermitteln,
            z.B. $P(\{1,2,4\}) = P(\{1\} \cup \{2\} \cup \{4\}) = P(\{1\}) + P(\{2\}) + P(\{4\})$.

        \subsection{Mehrstufige Zufallsversuche}
            \subsubsection{Beispiel}
            Eine faire Münze wird geworfen. Die Ergebnismenge eines Einzelversuch entspricht $\Omega_1 =\{$K, Z$\}$.
            Wird der Versuch nun $n$-mal wiederholt, ergibt sich eine neue Ergebnismenge $\Omega_n$. Aus $n=3$ folgt $\Omega_3=\{$KKK, KKZ, KZK, KZZ, ZKK, ZKZ, ZZK, ZZZ$\}$.
            Jedes Ergebnis $\omega_{1\dots8} \in \Omega_3$ beschreibt nun eine \textit{eindeutige} Abfolge von Münzwürfen, also ein 
            geordnetes Tripel aus Elementen von $\Omega_1$, z.B. (K,K,Z).
            
            Nun fragen wir uns nach dem Ereignis $A \in \Sigma_3 =\{$KKK, ZZZ$\}$, bei welchem alle Münzen gleich liegen.
            Die Ereignismenge $\Sigma_3$ entspricht hier $\mathcal{P}(\Omega_3)=\{\emptyset,\{$KKK$\}, \{$KKK$, $ZZZ$\}, \{$KKK$, $ZKZ$, $ZZZ$\}, \dots\}$

            \begin{center}
                \begin{tikzpicture}
                    [
                    scale=1,
                    level 1/.style={level distance=12mm,sibling distance=32mm},
                    level 2/.style={level distance=15mm,sibling distance=16mm},
                    level 3/.style={level distance=15mm,sibling distance=8mm},
                    solid node/.style={circle,draw,inner sep=1.5,fill=black},
                    edge/.style = {label=$\frac{1}{2}$,yshift=-3mm}
                    ]
                
                    \node(0)[solid node]{}  % first node

                    child {node(1)[solid node, label=left:K]{} 
                        child{node[solid node, label=left:KK]{}
                            child{node[solid node,label=below:{$\omega_1$}]{} edge from parent node [left, edge]{}}
                            child{node[solid node,label=below:{$\omega_2$}]{} edge from parent node [right, edge]{}}
                            edge from parent node [left, xshift=-2mm,edge]{}
                        } 
                        child{node[solid node, label=left:KZ]{}
                            child{node[solid node,label=below:{$\omega_3$}]{} edge from parent node [left, edge]{}}
                            child{node[solid node,label=below:{$\omega_4$}]{} edge from parent node [right, edge]{}}
                            edge from parent node [right, xshift=2mm,edge]{}
                        } 
                        edge from parent node [left, xshift=-2mm,edge]{}
                    } 
                    child{node(1)[solid node, label=right:Z]{}
                        child{node[solid node, label=right:ZK]{}
                            child{node[solid node,label=below:{$\omega_5$}]{} edge from parent node [left, edge]{}}
                            child{node[solid node,label=below:{$\omega_6$}]{} edge from parent node [right, edge]{}}
                            edge from parent node [left, xshift=-2mm,edge]{}
                        }
                        child{node[solid node, label=right:ZZ]{}
                            child{node[solid node,label=below:{$\omega_7$}]{} edge from parent node [left, edge]{}}
                            child{node[solid node,label=below:{$\omega_8$}]{} edge from parent node [right, edge]{}}
                            edge from parent node [right, xshift=2mm, edge]{}
                        }
                        edge from parent node [right, xshift=2mm, edge]{}
                    };
                \end{tikzpicture}
            \end{center}
                
            Weil wir wissen, dass es sich um eine faire Münze handelt, gilt $P(\{$K$\})=P(\{$Z$\})=0.5$.
            
            Mithilfe des Baumdiagramms und der \textbf{Pfadmultiplikationsregel} lassen sich nun die Wahrscheinlichkeiten 
            der einzelnen Pfade $\omega_1 \dots \omega_8$ errechnen. z.B. $P(\omega_2)=P(\{$K$\}) \cdot P(\{$K$\}) \cdot P(\{$Z$\}) = 0.125$.
            Hier gilt es anzumerken, dass $P(\omega_2)=P(\{$KKZ$\})$ die Wahrscheinlichkeit eines Ereignisses $A\in\Sigma_3$ ist und
            $P(${\{K\}$)$ die Wahrscheinlichkeit eines Ereignisses aus der Ereignismenge der Einzelversuche $\Sigma_1$.

            Mithilfe der \textbf{Summenregel} lässt sich nun $P(A)$ berechnen. Denn für $A = \{\omega_1,\omega_2,...,\omega_k\}$
            gilt $P(A)=P(\omega_1)+P(\omega_1)+\dots+P(\omega_k) = 0.25$ (Entspricht dem Addieren mehrerer Pfade).\\

            Solche mehrstufigen Zufallsversuche, bei welchen die Einzelversuche keinen Einfluss aufeinander haben, werden
            als \textbf{Bernoulli-Versuche} bezeichnet. Sie sind \textbf{stochastisch unabhängig.}

        \subsection{Bedingte Wahrscheinlichkeit}

            Die bedingte Wahrscheinlichkeit $P(A|B)$ ist die Wahrscheinlichkeit des Eintreten des 
            Ereignisses $A$ unter der Bedingung, dass das Eintreten des Ereignisses $B$
            bereits bekannt ist.
            Gegeben sei die Ergebnismenge $\Omega = \{\omega_1,\omega_2,\omega_3,\omega_4,\omega_5\}$ 
            und die Ereignisse $A=\{\omega_1,\omega_5\}$ und  $B=\{\omega_1, \omega_2\}$.

            \begin{center}
                \begin{tikzpicture}
                    [
                    scale=1,
                    level 1/.style={level distance=10mm,sibling distance=64mm},
                    level 2/.style={level distance=20mm,sibling distance=32mm},
                    solid node/.style={circle,draw,inner sep=1.5,fill=black},
                    ]
                
                    \node(0)[solid node]{}  % first node

                    child {node(1)[solid node, label=left:{$B=\{\omega_1, \omega_2\}$}]{}
                        child {node[solid node, label=below:{$A \cap B = \{\omega_1\}$}]{} edge from parent node[left]{$P(A|B)$}}
                        child {node[solid node, label=below:{$A^C \cap B = \{\omega_2\}$}]{} edge from parent node[right]{$P(A^C|B)$}}
                        edge from parent node[left,yshift=2mm]{$P(B)$}
                    }
                    child {node(1)[solid node, label=right:{$B^C=\{\omega_3, \omega_4, \omega_5\}$}]{}
                        child {node[solid node, label=below:{$A \cap B^C = \{\omega_5\}$}]{} edge from parent node[left]{$P(A|B^C)$}}
                        child {node[solid node, label=below:{$A^C \cap B^C = \{\omega_3, \omega_4\}$}]{} edge from parent node[right]{$P(A^C|B^C)$}}
                        edge from parent node[right,yshift=2mm]{$P(B^C)$}
                    }
                    ;
                \end{tikzpicture}
            \end{center}

            Der Entscheidungsbaum illustriert (gemäss Pfadmultiplikationsregel)
            \begin{equation}\label{eq:multiplikationsregel}
                P(A \cap B) = P(A|B) \cdot P(B)
            \end{equation}

            \subsubsection{Vierfeldertafel}
                \begin{center}
                    \renewcommand{\arraystretch}{1.5}
                    \begin{tabular}{c|c|c|c}
                            &  $A$            & $A^C$              & Gesamt  \\ \hline
                        $B$    & $P(A \cap B)$   & $P(A^C \cap B)$    & $P(B)$  \\ \hline
                        $B^C$  & $P(A \cap B^C)$ & $P(A^C \cap B^C)$  & $P(B^C)$ \\ \hline
                        Gesamt & $P(A)$          & $P(A^C)$             & 1
                    \end{tabular}
                \end{center}

                Mithilfe der Vierfeldertafel lassen sich fehlende Angaben ergänzen. 
                Sie lässt sich auf Wahrscheinlichkeiten oder Zahlenwerte anwenden.

                \paragraph{Beispiel.} Das Ereignis $Z$ stehe für die Tatsache, Zürich zu mögen.
                Das Ereignis $K$ dafür, Kaffee zu mögen. Die untenstehende Tabelle zeigt die
                Umfrageresultate in der Klasse.

                \begin{center}
                    \renewcommand{\arraystretch}{1.5}
                    \begin{tabular}{c|c|c|c}
                            & $Z$ &$Z^C$& Gesamt \\ \hline
                        $K$    & 11  &  3  & 14     \\ \hline
                        $K^C$  & 8   &  1  & 9      \\ \hline
                        Gesamt & 19  &  4  & 23
                    \end{tabular}
                \end{center}

                Die Wahrscheinlichkeiten $P(Z|K^C)=\frac{8}{9}$, $P(K^C|Z)=\frac{8}{19}$
                sowie $P(Z)=\frac{19}{23}$ und $P(K^C)=\frac{9}{23}$ lassen sich hier direkt aus der
                Tabelle auslesen, sowie mit dem Satz von Bayes (\ref{eq:satzBayes}) bestätigen:
                \begin{equation}
                    P(Z|K^C)=\frac{P(K^C|Z) \cdot P(Z)}{P(K^C)}=\frac{8}{9}=\frac{\frac{8}{\cancel{19}} \cdot \frac{\cancel{19}}{23}}{\frac{9}{23}}
                    =\frac{\frac{8}{\cancel{23}}}{\frac{9}{\cancel{23}}}=\frac{8}{9}
                \end{equation}

            \subsubsection{Satz von Bayes}
                Durch den Zusammenhang gegeben durch (\ref{eq:multiplikationsregel}) ergibt sich der
                Satz von Bayes:
                \begin{equation}\label{eq:satzBayes}
                    P(A|B)={\frac{P(A\cap B)}{P(B)}}={\frac {P(B\cap A)}{P(B)}}={\frac {P(B\mid A)\cdot P(A)}{P(B)}}
                \end{equation}

            \subsubsection{Satz der totalen Wahrscheinlichkeit}
                Entspricht der Addition der Wahrscheinlichkeiten der einzelnen Pfade, die so 
                die gesamte Ereignismenge abdecken.
                \begin{equation}
                    1 = P(A | B) \cdot P(B) + P(A^C | B) \cdot P(B) +  P(A | B^C) \cdot P(B^C) + P(A^C | B^C) \cdot P(B^C) 
                \end{equation}

        \subsection{Binomialverteilung}

            Die Binomialverteilung beschreibt die Anzahl der Erfolge in einer Serie von gleichartigen 
            und unabhängigen Versuchen, die jeweils genau zwei mögliche Ergebnisse haben („Erfolg“ oder „Misserfolg“). 
            Solche Versuchsserien werden auch \textbf{Bernoulli-Prozesse} genannt.

            Mit der Binomialverteilung lässt sich die Frage beantworten, wie gross die Wahrscheinlichkeit ist,
            bei $n$ Versuchen genau $k$ Erfolge erzielen, wenn $p\in[0,1]$ die Erfolgswahrscheinlichkeit darstellt.
            \begin{equation}\label{eq:binomialDistr}
                B (k, p, n) ={n \choose k} \cdot p^k \cdot (1-p)^{n-k} \;\; \textnormal{(für k < n).}
            \end{equation}

            \paragraph{Herleitung.} Sei \textbf{E} das Ereignis, einen Erfolg zu erzielen. Man möchte nun wissen, wie gross die Wahrscheinlichkeit ist,
            unter 10 Versuchen genau 4 Erfolge zu erzielen. Man muss sich nun fragen, wie viele Möglichkeiten 
            es gibt, dies zu erreichen:

            \begin {center}
                \renewcommand{\arraystretch}{2}
                \begin{tabular}{c l}
                    1. & $\underbrace{\textbf{EEEE}}_{4}\underbrace{\overline{EEEEEE}}_{6}$ \\
                    2. & $\underbrace{\textbf{EEE}}_{3}\underbrace{\overline{EEEEEE}}_{6}\underbrace{\textbf{E}}_{1}$ \\
                    3. & $\underbrace{\textbf{EE}}_{2}\underbrace{\overline{EEEEEE}}_{6}\underbrace{\textbf{EE}}_{2}$ \\
                    4. & $\underbrace{\textbf{EE}}_{2}\underbrace{\overline{EE}}_{2}\underbrace{\textbf{EE}}_{2}\underbrace{\overline{EEEE}}_{4}$ \\
                    \vdots &  \\
                
                \end{tabular}\\
            \end{center}

            Das entspricht der Frage, wie man 4 \textbf{E} auf 10 \glqq Plätze\grqq{}(=Anzahl Versuche) verteilen kann, ohne Berücksichtigung der Reihenfolge (siehe \ref{kombOhneWhd}). 
            Es gibt also genau $10 \choose 4$ Varianten. Jede Variante beinhaltet 4 Erfolge mit einer Wahrscheinlichkeit von $P(\textbf{E})$ und (10-4) 
            Misserfolge mit einer Eintrittswahrscheinlichkeit von $1-P(\textbf{E})$.
            
            
            \begin{center}
                \begin{tikzpicture}[
                    declare function={binom(\k,\n,\p)=\n!/(\k!*(\n-\k)!)*\p^\k*(1-\p)^(\n-\k)*100;}
                ]
                    \begin{axis}[
                        samples at={0,...,100},
                        width=12cm,
                        height=6cm,
                        yticklabel style={
                            /pgf/number format/fixed,
                            /pgf/number format/fixed zerofill,
                            /pgf/number format/precision=1
                        },
                        ybar=0pt, 
                        bar width=1,
                        xlabel={Anzahl Erfolge $k$},
                        ylabel={Eintrittswahrscheinlichkeit $p$ [\%]
                        }
                    ]
                    \addplot [fill=red, fill opacity=0.2] {binom(x,100,0.3)}; \addlegendentry{$p=0.3$}
                    \addplot [fill=blue, fill opacity=0.2] {binom(x,100,0.5)}; \addlegendentry{$p=0.5$}
                    \end{axis}
                \end{tikzpicture}
            \end{center}

            Stellt man die Funktion (\ref{eq:binomialDistr}) grafisch als Histogramm für konstante Erfolgswahrscheinlichkeiten $p$ und $n=100$ dar,
            lässt sich daraus ablesen, wie wahrscheinlich das Eintreten von einer beliebigen Anzahl Erfolgen ist. Bei $p=0.5$ besitzt die Situation mit 50 Erfolgen 
            die grösste Wahrscheinlichkeit, was dem Erwartungswert entspricht.

    \section{Kombinatorik}
        Mithilfe der Kombinatorik lässt sich die Anzahl der möglichen Ereignisse bestimmen.
        Mit der Laplace-Regel (\ref{eq:laplaceRule}), kann daraus auch die Wahrscheinlichkeit bestimmt werden.

        \subsection{Variationen}
            Anordnung von Objekten in einer \textbf{bestimmten Reihenfolge}.
            \subsubsection{ohne Wiederholung}
                Es sollen aus $n$ Objekten $k$ ausgewählt werden. Auf dem Taschenrechner entspricht dies der Funktion \textbf{nPr}.

                \begin{equation}\label{eq:nPr}
                    n \cdot (n-1) \cdot \dots \cdot (n-k+1) = \frac{n!}{(n-k)!}
                \end{equation}

                \paragraph{Beispiel.} Wieviele verschiedene \glqq Top-Ten\grqq{}-Ranglisten gibt es bei 18 Teilnehmer/innen?
                Auf dem ersten Platz können 18 verschiedene Leute sein. Dem zweiten stehen dann noch 17 zur Verfügung, usw.
                $$ \overbrace{18 \cdot 17 \cdot \dotsc \cdot 10 \cdot 9}^{\textnormal{10 Plätze}} 
                = \frac{18 \cdot 17 \cdot \dotsc \cdot 9 \dotsc \cdot \cancel{8 \cdot \dotsc \cdot 1}}{\cancel{8 \dotsc \cdot 1}}
                = \frac{18!}{8!}$$

            \subsubsection{Permutationen}
                Permutationen sind spezielle Variationen ohne Wiederholung: es werden alle Elemente benutzt.
                Setzt man in (\ref{eq:nPr}) $n=k$ folgt daraus
                \begin{equation}\label{eq:nFac}
                    1 \cdot 2 \cdot \dotsc \cdot (n-1) \cdot n = n!
                \end{equation}

                Die Permutation beantwortet beispielsweise die Frage, auf wieviele Arten man 10 Karten mischen kann.
            
            \subsubsection{mit Wiederholung}
                Die Anzahl der zur Auswahl stehenden Objekte verringert sich nicht, weil sie auch mehrfach verwendet werden dürfen.
                \begin{equation}
                    \underbrace{n \cdot \dotsc \cdot n}_{k\text{-mal}} = n^k
                \end{equation}

                Ein Beispiel für eine Variation mit Wiederholung ist ein klassisches Zahlenschloss. 
                Aus 4 Stellen mit 10 möglichen Ziffern ergeben sich $10^4 = 10\;000$ Möglichkeiten.
        
        \subsection{Kombinationen}
            Ohne Beachtung der Reihenfolge.
            \subsubsection{ohne Wiederholung}\label{kombOhneWhd}
                Gemäss (\ref{eq:nPr}) gibt es $\frac{n!}{(n-k)!}$ verschiedene Anordnungen, wenn man die Reihenfolge beachtet.
                Nun sind die ausgewählten $k$ Elemente ihrerseits noch auf $k!$ \glqq mischbar\grqq{}.
                Daraus ergibt sich der sogenannte \textbf{Binomialkoeffizient}, 
                der auf dem Taschenrechner als \textbf{nCr} bezeichnet wird:

                \begin{equation}\label{eq:nCr}
                    {n \choose k} = \frac{n!}{k!(n-k)!}
                \end{equation}

                \paragraph{Beispiel.} Wie viele Möglichkeiten gibt es, aus einer Gruppe von 100 Personen eine Delegation von 12 Personen auszuwählen?
                $${100 \choose 12} = \frac{100!}{12!\cdot(100-12)!} = 1.0504211 \cdot 10^{15}$$
                
                Ein anderes typisches Beispiel ist Lotto.

            \subsubsection{mit Wiederholung}
                Wenn die Reihenfolge nach wie vor nicht relevant ist, nun aber Elemente auch doppelt auftreten dürfen.
                \begin{equation}
                    \left(\!\!{n \choose k}\!\!\right) = {((k+n-1) \choose k}=\frac{(n+k-1)!}{k!}
                \end{equation}

                \paragraph{Beispiel.} Mit drei nicht unterscheidbaren Würfeln wird zeitgleich gewürfelt. 
                Es wären $6^3=216$ verschiedene Würfe möglich, wenn die Würfel voneinander unterscheidbar wären.
                Die Ereignisse (1,1,2), (1,2,1) und (2,1,1) sind aber hier nicht mehr unterscheidbar. 
                Somit sind nur noch
                $$ \left(\!\!{6 \choose 3}\!\!\right) = {8 \choose 3}=\frac{8!}{5!\cdot3!}=56 $$
                verschiedene Fälle möglich.        
\end {document}
    