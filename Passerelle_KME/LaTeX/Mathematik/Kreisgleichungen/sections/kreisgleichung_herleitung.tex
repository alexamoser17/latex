
Ein Kreis ist definiert als die Menge aller Punkte, die vom Mittelpunkt $M$
genau den Abstand $r$ haben. Ein Kreis ist also nicht zu verwechseln mit 
der Kreisscheibe; er hat wie eine Gerade keine Flächenausdehnung und besteht 
aus unendlich vielen Punkten.\\

Mit diesem Wissen betrachten wir nun einen Kreis $K$ mit dem Radius $r=2$
und dem Mittelpunkt $M=(2.5|2.5)$ sowie einen beliebigen Punkt $k$ auf diesem Kreis.
Da wir den Punkt $k$ nicht genauer einschränken, steht er stellvertretend für 
\textit{alle möglichen} Punkte auf dem Kreis.

\begin{center}
    \begin{tikzpicture}
        \begin{axis}[   samples=100,
                        inner axis line style={thick},
                        axis lines=center,
                        axis line style={-Latex},
                        ymax=5,
                        ymin=-1,
                        xmin=-1,
                        xmax=5,
                        width=8cm,
                        height=8cm
                        ]
        \addplot[domain=0.5:4.5,blue] {-(4-(x-2.5)^2)^0.5+2.5};
        \addplot[domain=0.5:4.5,blue] {(4-(x-2.5)^2)^0.5+2.5};

        \addplot[mark=none, black, dotted] coordinates {(2.5,0) (2.5,2.5)};
        \addplot[mark=none, black, dotted] coordinates {(0,2.5) (2.5,2.5)};
        \addplot[soldot] coordinates{(2.5,2.5)};

        \node at (axis cs:2.5, -0.3){$x_M$};
        \node at (axis cs:-0.3, 2.5){$y_M$};
        \node at (axis cs:-3, 1){$\lim\limits_{x \rightarrow p}{f(x)}$};

        \addplot[mark=none, black!50, very thick] coordinates {(2.5,2.5) (3.72,4.085)};
        \addplot[mark=none, black!50] coordinates {(3.72,2.5) (3.72,4.085)};
        \addplot[mark=none, black!50] coordinates {(3.72,2.5) (2.5,2.5)};
        \addplot[soldot] coordinates{(3.72,4.085)};

        \node at (axis cs:3.2, 2.3){$\Delta x$};
        \node at (axis cs:4, 3.2){$\Delta y$};

        \node at (axis cs:4.3, 4.2){$k \in K$};
        \node at (axis cs:2.4, 2.8){$M$};
        \node at (axis cs:3, 3.5){$r$};

        \end{axis}
    \end{tikzpicture}
\end{center}

Unser Kriterium, dass jeder Punkt $k$ des Kreises gleich weit von $M$
entfernt sein muss, halten wir mit folgendem Zusammenhang fest:
\begin{equation*}
    r=\overline{Mk}
\end{equation*}
Mithilfe des Satz des Pythagoras lässt sich die Distanz auch formulieren als:
\begin{align*}
    c^2&=a^2 + b^2\\
    r^2&=\Delta x^2 + \Delta y^2\\
    r^2&=(k_x-M_x)^2 + (k_y-M_y)^2
\end{align*}
    Ersetzen wir nun die Koordinaten des Punktes $k(k_x|k_y)$ mit der
    allgemeinen Bezeichnung $x$ und $y$, erhalten wir die Koordinatengleichung.
    Alle $(x,y)$-Paare, die diese Gleichung erfüllen, müssen also den gleichen Abstand $r$ vom
    Mittelpunkt $M$ haben und somit auf dem Kreis liegen.
\begin{equation}
    r^2=(x-M_x)^2 + (y-M_y)^2
\end{equation}
    Durch ausmultiplizieren unter Anwendung der zweiten binomischen Formel erhält man:
\begin{equation}
    r^2=(x^2-2\cdot M_x \cdot x+M_x^2) + (y^2-2\cdot y \cdot M_y + M_y^2)
\end{equation}
    Nach etwas Umordnen erhält man die allgemeine Form der Kreisgleichung mit den Parametern $A \dots D$, die 
    den Kreis eindeutig definieren.
\begin{align*}
    0&=\underbrace{1}_{A} \cdot x^2+\underbrace{1}_{A} \cdot y^2 + \underbrace{-2\cdot M_x}_{B}\cdot x + \underbrace{-2\cdot M_y}_{C} \cdot y + \underbrace{M_x^2+M_y^2-r^2}_{D}\\
    0&=Ax^2+Ay^2+Bx+Cy+D
\end{align*}

    Wichtig zu beachten ist, dass jeder Kreis in dieser Form darstellbar ist, jedoch nicht jede Gleichung dieser Form 
    einen Kreis repräsentiert.

