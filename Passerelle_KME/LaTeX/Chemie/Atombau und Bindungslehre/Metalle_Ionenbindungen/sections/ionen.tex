\section{Ionenbildung, Ionenbindung und Salze}
\begin{center}
    \emph{Salze sind Verbindungen aus je einem Metall und einem Nichtmetall.}
\end{center}
\subsection{Reaktionen zwischen Metallen und Nichtmetallen}
Bei der Reaktion zu einem Salz \emph{geben} die \emph{Metall-Atome} ihre wenigen \emph{Valenzelektronen ab} und verlieren dadurch eine Elektronenschale. Dadurch werden aus den Metall-Atomen \emph{positive Ionen (Kationen)}. Sie geben ihre Valenzelektronen an das beteiligte Nichtmetall ab, was dadurch zu einem \emph{positiven Ion (Anion)} wird. Die Ionen der Nichtmetalle und der Hauptgruppenmetalle erlangen durch die Reaktion in der Regel die \emph{Edelgaskonfiguration}
\begin{center}
    \begin{tabular}{r c c c c c c c}
        \textbf{Stoffe} & \ch[]{Na} & \ch[]{Cl} & \ch[]{->} & \ch[]{Na+} & \ch[]{Cl-} & \ch[]{->} & \ch[]{NaCl}\\
        \textbf{VE} & $2/8/1$ & $2/8/7$ & $e^-$ & $2/8$ & $2/8/8$ & & 
    \end{tabular}
\end{center}
In der Formel eines Ions wird das Ladungszeichen rechts oben neben das Symbol des Elements geschrieben\footnote{Beträgt die Ladungszahl $1$, wird sie nur durch $+$ oder $-$ ausgedrückt.}.
\begin{center}
    \begin{tabular}{c c c c}
        \ch[]{Na+} & \ch[]{Al3+} & \ch[]{O2-} & \ch[]{Cl-}
    \end{tabular}
\end{center}
Die positiven Metall-Kationen und die negativen Nichtmetall-Anionen, die durch die Elektronenübertragung während der Gasphase entstehen, ziehen sich gegenseitig an. Sie bilden ein \emph{Ionengitter}, in dem jedes Ion von einer bestimmten Zahl von Gegenionen umgeben ist. Die \emph{Koordinationszahl} gibt die Anzahl dieser Gegenionen an.\\
Durch die Zusammenlagerung der Kationen und Anionen entstehen die Kristalle eines Salzes (Ionenverbindung).

\subsection{Energieumsatz bei der Salzbildung}
Bei der Reaktion eines Metalls mit einem Nichtmetall muss für folgende Schritte Energie aufgewendet werden:
\begin{itemize}
    \item Die Metall-Atome aus ihrem \emph{Gitter lösen} (Sublimationsenergie).
    \item Die Moleküle des \emph{Nichtmetalls spalten} (Bindungsenergie).
    \item Die \emph{Metall-Atome ionisieren} (Ionisierungsenergie)
    \item \emph{Ionisierung der Nichtmetall-Atome}, jedoch ist es vom Nichtmetall abhängig, ob Energie frei oder verbraucht wird.
\end{itemize}
\begin{figure}[h]
    \centering
     \includegraphics[width = 80mm]{images/energieumsatz_salzbildung.jpeg}
    \caption{Energieumsatz bei der Salzbildung}
    \label{}
\end{figure}
Bei der \emph{Entstehung des festen Salzes} aus dem Ionen-Gas wird durch die Bildung des Ionengitters die \emph{Gitterenergie} frei. Weil sie die Summe der aufgewendeten Energien übertrifft, verläuft die \emph{Salzbildung exotherm}.

\subsection{Ionenladungen, Ionenradien und Gitterkräfte}
\subsubsection*{Ionenladungen}
\paragraph*{Ladung der Metall-Ionen} 
\begin{center}
    \emph{Die Ladungszahl des Ions der Hauptgruppen-Metalle entspricht in der Regel der Valenzelektronenzahl} (und damit der Gruppennummer).
\end{center}
\paragraph*{Ladung der Übergangsmetall-Ionen}
Die Übergangsmetalle können \emph{Ionen mit unterschiedlichen Ladungen} bilden, da sie Elektronen ihrer zweitäussersten Schale abgeben können.
\paragraph*{Ladung der Nichtmetall-Ionen}
Die Zahl der Elektronen, welche ein Nichtmetall aufnimmt, entspricht der Zahl, die ihm zur \emph{Edelgaskonfiguration fehlen}.
\begin{center}
    \emph{Für Nichtmetall-Ionen (ausser \ch[]{H-}) gilt: Ionenladung $=$ Gruppennummer minus acht.}
\end{center}

\subsubsection*{Ionenradien}
Die Atomradien der Anionen \emph{bleiben gleich}. Die Atomradien der \emph{Kationen} sind \emph{kleiner}, als die der Metall-Atome. Dies rührt von der Tatsache her, dass die Metall-Atome bei der Reaktion ihre Valenzelektronen abgeben.\\
Jedoch können sich Ionen mit gleicher Schalenzahl in ihrer Grösse unterscheiden, da eine höhere Kernladung die Valenzelektronen stärker anzieht.

\subsubsection*{Gitterkräfte}
Die Stärke der Ionenbindung lässt sich mit dem Gesetz von Coulomb ermitteln:
\begin{equation*}
    F = k \cdot \frac{Q_1 \cdot Q_2}{r^2} \hspace{15mm} k = 1~\text{falls nicht spezifiziert}
\end{equation*}
\begin{center}
    \emph{Die Gitterkräfte sind umso stärker, je höher die Ionenladungen und je kleiner die Ionenradien sind.}
\end{center}
Die \emph{Schmelztemperatur} des Salzes ist umso höher, je grösser die Ionenladungen sind.

\subsection{Salzformeln}
Die Salzformel ist eine Verhältnisformel und gibt das \emph{Zahlenverhältnis der Ionen} im Salzgitter an.\\
\ch[]{Al2S3} sagt also aus, dass das Verhältnis zwischen Aluminium-Ionen und Sulfid-Ionen $2:3$ beträgt.\\
\subsubsection*{Herleitung der Salzformel}
Das Zahlenverhältnis zwischen Kationen und Anionen ist immer so, dass die \emph{Summe aller Ionen null} ist.
\begin{gather*}
    x \cdot \ch[]{K+} + y \cdot \ch[]{S^{2-}} \ch[]{->} 2 \cdot \ch[]{K+} + 1 \cdot \ch[]{S^{2-}} \ch[]{->} \ch[]{K2S}\\
    2 \cdot \ch[]{Cu^x} + 1 \cdot \ch[]{O^{2-}} \ch[]{->} 2 \cdot \ch[]{Cu+} + 1 \cdot \ch[]{O^{2-}} \longrightarrow \ch[]{Cu2O}
\end{gather*}
In der Formel eines binären Salzes steht das Symbol des Metalls vor dem Symbol des Nichtmetalls.

\subsection{Benennung der Salze}
Der Namen eines binären Salzes bildet sich aus dem Namen des Metalls, gefolgt von der Bezeichnung für das Nichtmetall-Ion. Diese ergibt sich aus dem deutschen oder lateinischen Namen des Nichtmetalls und der Endung \textit{-id}.\\
Da ein Hauptgruppen-Metall in der Regel nur mit einem Nichtmetall eine Bindung eingeht, ist der Name auch ohne die Angabe des Zahlenverhältnisses eindeutig.\\
Bei Salzen mit Metallen, die mehrere Ionen bilden können, wird die Ladungszahl als römische Ziffer in Klammern, gefolgt von einem Bindestrich in den Namen eingebunden.
\begin{center}
    \ch[]{Fe_2^{\color{red}{$3$}}+} \ch{O^{2-}3} $\longrightarrow$ Eisen(\textcolor{red}{\rom{3}})-oxid (gesprochen: Eisen\textcolor{red}{drei}oxid)
\end{center}

\subsection{Reaktionsgleichung für Salzbildung}
Um die Reaktionsgleichung für die Bildung eines Salzes aufzustellen, wird die Salzformel aus den Ionenladungen hergeleitet. Dann müssen die Koeffizienten so gewählt werden, dass sich die Ladungen gegenseitig aufheben.
\begin{align*}
    \ch[]{Fe}^{\textcolor{red}{3+}} + \ch[]{S^{\color{blue}{2-}}} & \ch[]{->} \ch[]{Fe_xS_y}\\
    \textcolor{red}{2}\ch[]{Fe^{3+}} + \textcolor{blue}{3}\ch[]{Fe^{2-}} & \ch[]{->} \ch[]{Fe_{\color{red}{2}}S_{\color{blue}{3}}}
\end{align*}

\subsection{Salze mit Molekül-Ionen}
Salze können auch aus einem Metall und einem Molekül-Ion gebildet werden.
\begin{equation*}
    \ch[]{!(Metall)( Na+ )} + \ch[]{!(Molekül-Ion)( ClO- )} \ch[]{->} \ch[]{!(Salz)(NaClO)}
\end{equation*}
Molekül-Ionen sind geladene Teilchen aus kovalent gebundenen Atomen. Für ihre Ladung gilt:
\begin{equation*}
    \text{Ladung} = (\text{Zahl der VE aller Atome}) - 2 \cdot (\text{Zahl der EP des Molekül-Ions})
\end{equation*}
Die Meisten Molekül-Ionen haben eine negative Ladung zwischen $1-$ und $3-$.

\begin{table}[h]
    \centering
    \caption{Die wichtigsten Molekül-Ionen}
    \vspace{2mm}
    \begin{tabular}{c c c c c c}
        Carbonat & Chlorit & Chlorat & Perchlorat & Cyanid & Hydroxid\\
        \ch[]{CO_2^3+} & \ch[]{CLo_2-} & \ch[]{ClO_3-} & \ch[]{ClO_4-} & \ch[]{CN} & \ch[]{OH-}\\
        \hline \hline
        Nitrit & Nitrat & Phosphat & Sulfit & Sulfat & Ammonium\\
        \ch{NO_2-} & \ch[]{NO_3-} & \ch[]{PO_4^3-} & \ch[]{SO_3^2-} & \ch[]{SO_4^2-} & \ch[]{NH_4+}
    \end{tabular}
\end{table}