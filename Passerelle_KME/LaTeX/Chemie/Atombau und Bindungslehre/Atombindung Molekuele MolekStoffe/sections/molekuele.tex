\section{Moleküle}
\subsection{Herleitung der Molekülformel}
Die Molekülformel beschreibt die \emph{Zusammensetzung eines Moleküls}. Sie setzt sich aus den Symbolen der gebundenen Atome und deren Index\footnote{Ist der Index gleich $1$, wird er nicht geschrieben.}. Der Index gibt die Anzahl der gebundenen Atome in einem Molekül an.

\subsection{Strukturformel}
Die Strukturformel gibt Aufschluss darüber, wie die Moleküle verknüpft sind, macht aber keine verbindliche Aussage über die Bindungswinkel (d.h. die Gestalt des Moleküls).
\begin{center}
    \chemfig[atom sep = 0.8cm]{H-O-H}
\end{center}
Bindungen zwischen Molekülen werden mit Strichen dargestellt.
\begin{tabbing}
    Einfachbindungen \qquad \= \chemfig{A-B}\\
    Doppelbindungen \> \chemfig{A=B}\\
    Dreifachbindungen \> \chemfig{A~B}
\end{tabbing}

\subsubsection*{Strukturformel komplexer Moleküle}
Kann eine Molekül das Oktett nicht erreichen, weil ihm benötigte Anzahl Valenzelektronen fehlt, geht es die \emph{maximal möglichen Bindungen} ein. Bor hat beispielsweise nur $3$ Valenzelektronen und müsste somit $5$ Bindungen eingehen. Da dies aber unmöglich ist, ist der Atomrumpf von Bor nur von $6$ Elektronen umgeben.\\
Atome können das Oktett erreichen, indem sie \emph{ungepaarte Elektronen} oder \emph{Elektronenpaare als Bindungselektronen zur Verfügung stellen}. Ein solches Verhalten zeigen viele Nichtmetalle gegenüber Sauerstoff.
\begin{center}
    \schemestart
        \chemfig{\charge{0=\|, 90=\|, 180=\., 270=\|}{Cl}}
        \arrow
        \chemfig{\charge{0=\.}{H}
        -[,0.5,,,,draw=none]
        \textcolor{blue}{\charge{[{.style={draw=blue,fill=blue}}, {|style={draw=blue,fill=blue}}]0=\., 90=\|, 180=\., 270=\|}{H}}
        -[,0.5,,,,draw=none]
        \charge{180=\.}{C}
        (-[2,0.8]\textcolor{blue}{\charge{[{|style={draw=blue,fill=blue}}]0=\|, 90=\|, 180=\|}{O}})
        (-[6,0.8]\textcolor{blue}{\charge{[{|style={draw=blue,fill=blue}}]0=\|, 180=\|, 270=\|}{O}})
        -[,0.8]\textcolor{blue}{\charge{[{|style={draw=blue,fill=blue}}]0=\|, 90=\|, 270=\|}{O}}}
    \schemestop
\end{center}
Drei von den vier bindenden Elektronenpaaren zwischen \ch{O} und \ch{Cl} stammen von Chlor-Atom. Dieses müsste eigentlich nur eine Bindung eingehen, um das Oktett zu erreichen, jedoch stellt es seine drei Elektronenpaare zur Verfügung, um weitere Bindungen einzugehen.  

\subsection{Molekülgestalt}
Die \emph{räumliche Struktur} der Moleküle \emph{beeinflusst viele Eigenschaften} molekularer Stoffe. Die Molekülgestalt eines Stoffes lässt sich experimentell ermitteln und mithilfe eines Modells\footnote{Modelle zur Darstellung der Molekülgestalt wären das Kalotten- oder Kugel-Stab-Modell} oder der Keilstrichformel darstellen.
\subsubsection*{Keilstrichformel}
Die Keilstrichformel ähnelt von der Darstellung der Strukturformel. Jedoch lassen sich gemeinsame Elektronenpaare je nach Lage unterschiedlich darstellen.
\begin{tabbing}
    Bindung nach vorne: \qquad\=\chemfig{A<B}\\
    Bindung nach hinten:\>\chemfig{A<:B}
\end{tabbing}
\newpage
\subsection{Herleiten der Molekülgestalt}
Die Elektronenwolken bindender und freier Elektronenpaare stossen sich gegenseitig ab und ordnen sich mit dem \emph{grösstmöglichen Abstand} an. Dabei verhalten sich Mehrfachbindungen grundsätzlich wie Einfachbindungen. Die Wolke eines freien Elektronenpaares ist etwas grösser als die eines bindenden.\\
\begin{table}[h]
    \caption{Molekülgestalt und Bindungswinkel}
    \setchemfig{atom sep = 0.3cm}
    \centering
    \renewcommand{\arraystretch}{1.5}
    \begin{tabular}{l c c c c c}
        Allg. Formel & \chemfig{-X(-[2])(-[6])-} & \chemfig{-\lewis{2,X}(-[6])-} & \chemfig{\lewis{13,X}(-[5])(-[7])} & \chemfig{X(-[2])(-[5])(-[7])} oder \chemfig{X(=[2])(-[5])(-[7])} & \chemfig{=X=} oder \chemfig{-X~}\\
        \hline
        Bindungspartner & $4$ & $3$ & $2$ & $3$ & $2$\\
        freie EP & $0$ & $1$ & $2$ & $0$ & $0$\\
        Summe & \textbf{4} & \textbf{4} & \textbf{4} & \textbf{3} & \textbf{2} \\
        \hline
        Bindungswinkel & ca. \SI{109}{\degree} & ca. \SI{107}{\degree} & ca. \SI{104}{\degree} & \SI{120}{\degree} & \SI{180}{\degree}\\
        \hline
        Kalotten-Modell 
        & \begin{tikzpicture}[scale = 0.5, transform shape]
            \input{molecules/molecule_orientation/tetraedrisch.tex}
        \end{tikzpicture} 
        & \begin{tikzpicture}[scale = 0.5, transform shape]
            \input{molecules/molecule_orientation/pyramidal.tex}
        \end{tikzpicture} 
        & \begin{tikzpicture}[scale = 0.5, transform shape]
            \input{molecules/molecule_orientation/planar_gewinkelt.tex}
        \end{tikzpicture} 
        & \begin{tikzpicture}[scale = 0.5, transform shape]
            \input{molecules/molecule_orientation/planar.tex}
        \end{tikzpicture} 
        & \begin{tikzpicture}[scale = 0.5, transform shape]
            \input{molecules/molecule_orientation/linear.tex}
        \end{tikzpicture} 
    \end{tabular}
\end{table}

\subsection{Molekülgestalt und Molekülpolarität}
Moleküle mit \emph{polaren Bindungen} sind \emph{Dipole}. Sie besitzen immer einen positiv und negativ geladenen Pol. Während alle zweiatomigen Moleküle mit polaren Bindungen Dipole sind, können sich die Bindungspolaritäten bei grösseren Molekülen gegenseitig aufheben. \emph{Fallen die Schwerpunkte der positiven und negativen Partialladungen zusammen}, ist das Molekül \emph{kein Dipol}.
\begin{table}[h]
    \centering
    \renewcommand{\arraystretch}{1.5}
    \begin{tabular}{c|c}
        Dipol & kein Dipol\\
        &\\
        \chemfig{
            \charge{90:4pt=$\partialpos$}{H}
            -[1]\charge{45=\|, 135=\|, 90:8pt=$\partialneg$}{O}
            -[7]\charge{90:4pt=$\partialpos$}{H}
        }
        & \chemfig{
            \charge{135=\|, 225=\|, 70:4pt=$\partialneg$}{O}
            =\charge{90:4pt=$\partialpos$}{C}
            =\charge{45=\|, 315=\|, 100:4pt=$\partialneg$}{O}
        }
    \end{tabular}
\end{table}

\subsection{Namen binärer Molekülverbindungen}
In der Molekülformel steht das Symbol\footnote{Abkürzung des Elements, \ch{Cl} für Chlor.} des Elements mit der höheren Elektronegativität meist hinten. Die Symbole der Elemente der \rom{4}. und der \rom{5}. Gruppe stehen immer vorne.\\
Der Name einer binären Molekülverbindungen wird aus den Namen der Elemente und der Endung \textit{-id} gebildet. Für das zweite Element wird oft eine aus dem lateinischen Namen abgeleitete Bezeichnung verwendet. Die Zahl der Atome eines Moleküls wird durch das vorangestellte griechische Zahlwort angegeben.
\begin{center}
    \ch{N2O5}: \qquad \textcolor{blue}{Di}stickstoff\textcolor{blue}{penta}ox\textcolor{darkred}{id}
\end{center}