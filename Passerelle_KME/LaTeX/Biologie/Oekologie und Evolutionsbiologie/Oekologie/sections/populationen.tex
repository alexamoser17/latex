\section{Populationen}
\subsection{Bedeutung, Grösse und Verteilung der Population}
Eine \emph{Population} ist die \emph{Fortpflanzungsgemeinschaft} aller Individuen einer Art, die in einem Ökosystem leben.\\
Jede Population funktioniert nach gewissen Regel und Gesetzmässigkeiten, die das Verhalten des Einzelnen beeinflussen. Das Zusammenleben in einer Population hat für jedes Individuums zwei Seiten: \emph{Kooperation} und \emph{Konkurrenz}.

\subsubsection*{Variabilität und Genpool}
Je grösser der Genpool, desto höher ist die Zahl der möglichen Varianten und somit die Variabilität der Population. Durch \emph{Mutation} wird der \emph{Genpool grösser}, durch \emph{Selektion kleiner}.\\
Die Variabilität des Genpools erhöht die Überlebenschance einer Population gegenüber ändernden Umweltfaktoren.\\
Nutztiere und Nutzpflanzen sind vom Menschen auf eine bestimmte Leistung gezüchtet und selektioniert und weisen dadurch eine sehr schmale genetische Variabilität auf.

\subsubsection*{Merkmale einer Population}
\begin{flushleft}
    \renewcommand{\arraystretch}{1.5}
    \begin{tabular}{l l}
        Populationsgrösse & Gesamtzahl der Individuen\\
        Populationsdichte & Individuenzahl pro Flächeneinheit\\
        Räumliche Verteilung & Verteilung der Individuen im Siedlungsgebiet\\
        Altersstruktur & Anteile der verschiedenen Altersgruppen\\
        Geburten- und Sterberate & Geburten und Sterbefälle in einem Zeitabschnitt\\
        Zu- und Abwanderung & Anzahl Zu- und Abwanderer in einem Zeitabschnitt
    \end{tabular}
\end{flushleft}

\subsubsection*{Bestimmung der Populationsgrösse}
\paragraph*{Pflanzen} werden gezählt, indem man die Individuen auf \emph{repräsentativen Probeflächen} zählt und dann auf die Gesamtfläche hochrechnet.
\paragraph*{Tiere} lassen sich meistens nur schwer zählen. Oft setzt man \emph{Fallen} ein aber auch Jagdstatistiken, Kot, Spuren oder Nester geben Aufschluss auf die Individuenzahl.
\paragraph*{Fang-Widerfang-Methode} bezeichnet ein Vorgehen, bei dem ein Teil der Population markiert werden. Die markierten Tiere werden freigelassen und sobald sie sich wieder unter den Rest der Population gemischt haben wird ein neuer Teil gefangen. Das Verhältnis zwischen markierten und unmarkierten Tieren im zweiten Fang, gibt einen Aufschluss über die Populationsgrösse.

\newpage

\subsubsection*{Räumliche Verteilung}
Die Verteilung von Lebewesen im Habitat wird durch \emph{abiotische Faktoren}, \emph{Lebensweise} und \emph{Verhalten} bestimmt.
\begin{figure}[h]
    \centering
    \includegraphics[width = 14cm]{images/verteilungsmuster.jpeg}
    \caption{Unterschiedliche Verteilungsmuster der Lebewesen einer Population}
    \label{}
\end{figure}

\subsection{Populationswachstum}
Das Wachstum einer Population ist von der Zahl der Geburten und Todesfälle abhängig. Als Mass für die Wachstumsgeschwindigkeit dient die relative Wachstumsrate $r$.
\begin{equation*}
    \text{Relative Wachstumsrate } r = \frac{\text{Geburtenzahl} - \text{Sterbefälle}}{\text{Indiviuenzahl}}
\end{equation*}

\subsubsection*{Logistisches Wachstum}
Früher oder später endet das exponentielle Wachstum jeder Population, da ein Umweltfaktor \emph{limitierend} wirkt und nur eine gewisse Populationsgrösse unterstützen kann. Ein solches Wachstum richtete sich nach der möglichen Versorgung und heisst deswegen logistisch. Die maximale Grösse einer Population wird als \emph{Umweltkapazität} $K$ bezeichnet.
\begin{figure}[h]
    \centering
    \includegraphics[height = 6cm]{images/logistisches_wachstum.jpeg}
    \caption{Populationsgrösse und Wachstumsrate beim logistischen Wachstum}
    \label{}
\end{figure}
Das Populationswachstum wird durch \emph{wachstumsbegrenzende Faktoren} verlangsamt. Die Gesamtheit der wachstumsbegrenzenden Faktoren heisst \emph{Umweltwiderstand}. Je grösser eine Population wird desto stärker wird der Umweltwiderstand. Wachstumsbegrenzende Faktoren können sein: Nahrungsmangel, Vermehrung der Feinde, Abnahme des Lebensraums, Epidemien o.ä.
\newpage
Das idealisierte Populationswachstum sieht vor, dass eine Population exponentiell wächst, bis es die hälfte der Umweltkapazität erreicht hat. Ab diesem Punkt nimmt die Wachstumsrate ab und ist bei $K$ null. Dieses Verhalten setzt aber vor, dass der limitierende Umweltfaktor schon vor Erreichen von $K$ wirksam wird.\\
\paragraph*{Natürliche Populationen} entwickeln sich meist nicht nach dem idealisierten Modell. Übersteigt die Populationsgrösse $K$, fällt danach unter $K$ und steigt dann wieder, nennt man diesen Vorgang \emph{Massenwechsel}.
\begin{figure}[h]
    \centering
        \begin{tikzpicture}
            \matrix[row sep = 0.5cm, column sep = 0.5]{
                \node[] (pic1) {\includegraphics[width = 4cm]{images/populationskurve1.JPEG}};
                & \node[] (pic2) {\includegraphics[width = 4cm]{images/populationskurve2.JPEG}};
                & \node[] (pic3) {\includegraphics[width = 4cm]{images/populationskurve3.JPEG}};\\
            };

            \node[text width = 4cm, below = 0.5cm of pic1] {\scriptsize Exponentielles Wachstum bis $K$, Übergang zu logistischem Wachstum mit abnehmendem Massewechsel.};
            \node[text width = 4cm, below = 0.5cm of pic2] {\scriptsize Exponentielles Wachstum und schlagartig einsetzener Umweltwiderstand mit Massensterben.};
            \node[text width = 4cm, below = 0.5cm of pic3] {\scriptsize Exponentielles Wachstum und exponentielle Abnahme vor erreichen von $K$.};
        \end{tikzpicture}
    \caption{Populationskurven}
    \label{}
\end{figure}

\subsection{Regulation der Populationsdichte}
Das Wachstum einer Population wird durch die Umwelt reguliert. Man unterscheidet zwischen \emph{dichteabhängigen} und \emph{dichteunabhängigen} Umweltfaktoren. Dichteabhängige Umweltfaktoren wirken mit zunehmender Populationsgrösse immer stärker, dichteunabhängige nicht.

\subsubsection*{Dichteunabhängige Faktoren}
Dichteunabhängige Faktoren wie \emph{Licht}, \emph{Temperatur}, \emph{Wasser}, \emph{wind} und \emph{Bodeneigenschaften} wirken sich auf eine Population unabhängig von ihrer Dichte aus. Sie \emph{limitieren} das Wachstum einer Population, regulieren es aber nicht direkt.

\subsubsection*{Dichteabhängige Faktoren}
Dichteabhängige Faktoren wirken \emph{regulierend} auf die Populationsdichte. Dichteabhängige Faktoren sind:
\begin{itemize}
    \item \emph{Nahrungsangebot}
    \item \emph{Spezifische Feinde} und \emph{Parasiten}
    \item \emph{Epidemische Krankheiten}
    \item \emph{Sozialer Stress}
    \item \emph{Abwanderung}
\end{itemize}

\subsubsection*{Innere Faktoren}
Periodisch auftretende Massenwechsel können auch ohne erkennbar äussere Faktoren erfolgen. Der Grund für solche Schwankungen sind \emph{innere Faktoren} wie \emph{Änderungen im Genpool}.

\subsection{Wechselwirkung zwischen Räuber und Beute}
Für Populationen eines einfachen Räuber-Beute-Systems gelten die Volterra-Regeln:
\begin{flushleft}
    \renewcommand{\arraystretch}{1.5}
    \begin{tabular}{p{2.6cm} p{11.4cm}}
        1. Volterra-Regel & Die \emph{Grösse} beider Populationen \emph{schwankt periodisch}, wobei die Schwankungen der Räuberpopulationen den Schwankungen der Beutepopulationen verzögert folgen.\\
        2. Volterra-Regel & Trotz periodischer Schwankungen sind die \emph{Mittelwerte} der Populationsgrössen von Räuber und Beute über einen längeren Zeitraum \emph{konstant}.\\
        3. Volterra-Regel & Werden die Populationen dezimiert, \emph{erholt} sich die \emph{Beutepopulation schneller} als die Räuberpopulation.\\
    \end{tabular}
\end{flushleft}
Der Räuber ist von der Beute abhängig. In natürlichen Systemen kann ein Räuber seine Beute nicht ausrotten.

\subsection{Vielfalt und Stabilität}
Eine Biozönose verändert sich. Sie ist umso stabiler, je langsamer und regelmässiger sich ihre Zusammensetzung verändert. Das biologische Gleichgewicht in einem Ökosystem ist umso besser, je grösser die Artenvielfalt und Individuenzahl sind und je vernetzter die Nahrungsbeziehungen sind.\\
In \emph{heterogenen Ökosystemen} mit \emph{starker Gliederung} ist die \emph{Zahl} der \emph{Nischen grösser} als in ungegliederten Ökosystemen.\\
Ökosysteme mit \emph{konstanten} und \emph{durchschnittlichen} abiotischen Ökofaktoren und starker räumlicher Gliederung haben eine \emph{hohe Artenvielfalt}.